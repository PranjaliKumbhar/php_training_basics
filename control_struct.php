<html>
  <head>
    <title>PHP Basics </title>
  </head>
  <body>
    <!--control structure-->
    <h1> control structure in PHP </h1>
    <h1>comparison opr</h1>
    <p>
      equal to == <br>
      identical === <br>
      compare > < >= <= <> <br>
      not equal != <br>
      not identical !== <br>
    </p>
    <h1> logical opr</h1>
    <p>
      And && <br>
      or || <br>
      Not ! <br>
    </p>

    <?php
      $no=30;
      //if stmt
      if($no === 20)
      {
        echo $no;
      }
      else if($no < 100)
      {
        echo $no ." " .'is less than 100'."<br>";
      }
      else
      {
        echo $no;
      }
      //comparison and logical opr
      //switch stmt
      $no = 40;
      switch($no)
      {
        case 10:
          echo "It is 10";
        break;
        case 20:
          echo "It is 20"."<br>";
        break;
        case 30:
          echo "It is 30"."<br>";
        break;
        case 40:
          echo "It is 40"."<br>";
        break;
        
        default:
          echo "default block"."<br>";
        break;
      }
      //while loop 
      echo "while loop"."<br>";
      $i =0;
      while($i < 10) {
        echo $i .'<br>';
        $i++;
      }
      //for loop
      echo "for loop"."<br>";
      for($i=0;$i<=10;$i++)
      {
        echo $i .'<br>';
      }
      //foreach loop
      echo "foreach loop"."<br>";
      $no = [10,20,30,40,50,60];
      foreach($no as $n)
      {
        echo $n ."<br>";
      }
    ?>

  </body>
</html>