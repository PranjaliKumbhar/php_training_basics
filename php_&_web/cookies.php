<?php

$cookiename = "testcookie";
$value = 100;
$expiration = time() + (60*60*24*7);
setcookie($cookiename,$value,$expiration,'/PHP_Basics/php_&_web');

?>

<html>
  <head>
    <title>
      PHP and Web - cookies
    </title>
  </head>
  <body>
    <?php
      if(isset($_COOKIE['testcookie']))
      {
        $value = $_COOKIE['testcookie'];
        echo $value;
      }
    ?>
    
  </body>
</html>

