<html>
  <head>
    <title>PHP Basics - Array </title>
  </head>
  <body>
    <!--Array-->
    <h1> Arrays in PHP </h1>
    <?php
      //2 ways to declare 
      $no_list = array(1,2,'<h1>Hello</h1>','100000');
      $no = [10,20,30,40];

      echo $no_list[0];
      //print_r($no_list);

      //Types of array (Associative array)
      $no = array(1,2,3,4);
      echo "<br>";
      print_r($no);
      //key => value
      $no_new = array("fname" => 'pranjali',"lname" => 'kumbhar');
      print_r($no_new); 
      echo "<br>";
      //refering data by label/key 
      echo $no_new['fname'] ." " . $no_new['lname'];


      
      
     
    ?>
  </body>
</html>