<?php

class Car {
  
  var $wheels = 4;

  function MoveWheels()
  {
    $this->wheels = 10;
    echo "Move Wheels";

  }
}


//instantiate a class
$car1 = new Car();
$car2 = new Car();

//call function
// $car1->MoveWheels();
// echo $car1->wheels;


//class inheritance

class Motor extends Car
{
  var $wheels = 2;
}

$obj = new Motor();
echo $obj->wheels;

//constructor
class BaseClass
{
  protected $var2 = 20;
  function __construct()
  {
    echo "In BaseClass constructor"."<br>";
    
  }
}
class SubClass extends BaseClass {

  //access specifier
  public $var1 = 10;
  static $cnt = 1;
  
  private $var3 = 30;

  function __construct()
  {
    //call to base class constructor
    parent::__construct();
    echo $this->var3 . $this->var2."<br>";
    echo "In SubClass constructor"."<br>";
  }
  
  
}

$ob = new SubClass();
echo $ob->var1;

//way to access static variable
//SubClass -> where it is located 
echo SubClass::$cnt;












//to check class is exists or not /created properly 
// if(class_exists("Car"))
// {
//   echo "class is exists";

// }

//to check method is exists or not /created properly 
// if(method_exists("Car","MoveWheels"))
// {
//   echo "method is exists";

// }


?>


<html>
  <head>
    <title>
      PHP and Web - OOP in PHP
    </title>
  </head>
  <body>
    
  </body>
</html>

