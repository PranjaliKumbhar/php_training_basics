<html>
  <head>
    <title>PHP Basics </title>
  </head>
  <body>
    <!--inbuilt functions-->
    <h1> inbuilt functions </h1>
    <?php
      //built in math functions
      echo pow(2,3)."<br>";
      echo rand(1,100)."<br>";
      echo sqrt(100)."<br>";
      echo ceil(100.6789)."<br>";
      
      echo floor(100.6789)."<br>";
      echo round(100.6789)."<br>";
      
      //string func 
      $str = "This is a test string";
      echo strlen($str)."<br>";
      echo strtoupper($str)."<br>";
      echo strtolower($str)."<br>";

      //array functions
      $list = [10,20,475757,9586905,5346,6776];
      echo max($list)."<br>";
      echo min($list)."<br>";
      sort($list);
      print_r($list);

    ?>
  </body>
</html>