<html>
  <head>
    <title>PHP Basics </title>
  </head>
  <body>
    <!--custom functions-->
    <h1> custom functions </h1>
    <?php
      //custom functions
      function init()
      {
        add();
      }
      function add()
      {
        echo "I'm in function PHP custom function"."<br>";
      }
      init();
      //pass parameters in function
      function demo($no1,$no2)
      {
        //echo $no1 + $no2;
        return $no1 + $no2;

      }
      $res = demo(10,20);
      echo $res;

      //global variables

      $str = 'test outside'; //global scope
      function disp()
      {
        global $str; //to make var global 

        $str = 'inside fun'; //local scope
      }
      echo $str."<br>";
      disp();
      echo $str."<br>"; //value is changed when made as global 
      
      //constant
      //use to defin var as constant
      define("Name",1000);
      echo Name."<br>";
      //2nd way 
      const no =11;
      echo no;

      
      
     
    ?>
  </body>
</html>